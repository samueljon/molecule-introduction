# Introduction

This lab demonstrates the journey of moving simple ansible playbook to a role developed with the aid of molecule https://molecule.readthedocs.io/en/stable/

```bash
# Creating role skeleton with molecule
molecule init role -r my-new-role --driver=docker
# Creating role skeleton with molecule
molecule init role -r my-new-role --driver=docker


molecule init scenario -r my-role-name --driver=docker
```