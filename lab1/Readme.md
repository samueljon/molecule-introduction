# outline

# Init a new role with molecule
molecule init role -r my-new-role

# Add molecule functionality to existing role
molecule init scenario -r my-role-name